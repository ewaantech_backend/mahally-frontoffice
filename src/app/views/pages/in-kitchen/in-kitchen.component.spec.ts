import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InKitchenComponent } from './in-kitchen.component';

describe('InKitchenComponent', () => {
  let component: InKitchenComponent;
  let fixture: ComponentFixture<InKitchenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InKitchenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InKitchenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
