import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ValidationService } from 'src/app/services/validation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-bump-order-confirmation',
  templateUrl: './bump-order-confirmation.component.html',
  styleUrls: ['./bump-order-confirmation.component.css']
})
export class BumpOrderConfirmationComponent implements OnInit {

  order_id: number;
  order_status: number;
  update_status: number;
  orderList=[];
  OrderViewList;
  redirectoPage;
  langindex:any=0;
 


  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private SpinnerService: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    this.route.queryParams.subscribe(params => {
      this.order_id = params['order_id'];
      this.order_status= params['order_status'];
      this.redirectoPage=params['redirectoPage'];
  
      
      if (this.order_id !== undefined && this.order_status !== undefined) {
        // this.router.navigate(['/pending-orders']);
      }
      else{
      this.router.navigate(['/pending-orders']);
      }
    });
 
    this.doFetchOrderListByOrderid();
    this.doFetchOrderViewDetails();

  }
  

  doFetchOrderViewDetails() {
    this.SpinnerService.show(); 
    this.apiService.doOrderViewList(this.order_id)
      .subscribe(
        (data: any) => {
          this.OrderViewList =data.data.orders;
         console.log(this.OrderViewList);
        },

        error => {
          this.toastr.error(error.error.error.msg);
          console.log(error);
        }
      );
      this.SpinnerService.hide();  
      } 

  doFetchOrderListByOrderid() {
    this.apiService.doGetOrderListByOrderid(this.order_id)
      .subscribe(
        (data: any) => {
          this.orderList =data.data.orders;
         
          // this.toastr.success('Order Fetched');
     
        },
        error => {
          this.toastr.error(error.error.error.msg);
          console.log(error);
        }
      );
      }    
        

      doBump(){
        this.router.navigate(['/bump'],{ queryParams: { order_id: this.order_id,order_status:this.order_status },});
      }   
      doPrint(){
        window.print();
      }

      // doPrint(){
      
      //   //window.print();
      // //  var divContents = document.getElementById("prntPreview").innerHTML;
      //   var a = window.open('', '', 'height=auto, width=auto');
      //   var contentHlmrow='';

      //   a.document.write('<html>');
      //   a.document.write('<body style="-webkit-print-color-adjust: exact;" onload="window.print()"> ');
      //   a.document.write('<h3 style="text-align:center; font-size:18px;">'+'Restaurant Name'+'</h3>');
      //   a.document.write('<h3 style="text-align:center; font-size:15px;">'+this.OrderViewList.customer[0].email+'<br/>'+'9874563120'+'</h3>');
      //   // a.document.write('<span>'+'9874563120'+'</span>');
       
      //   a.document.write('<br/>');
      //   a.document.write('<span style="font-size:18px;"> Restaurant Bill Receipt </span><br/>');
      //   a.document.write('<span>'+'Receipt Number :'+this.order_id+'</span><br/>');
      //   a.document.write('<span>'+'Customer Name :'+this.OrderViewList.customer[0].name +'</span><br/>');
      //   var dateformt = new Date(this.OrderViewList.created_at).toLocaleDateString("en-US");
      //   a.document.write('<span>'+'Date :'+dateformt+'</span><br>');
      //   a.document.write('<table style="width:100%; align:center;">');
      //   a.document.write('<tr style="background-color:LightGray;"> <th style="width:250px;">Item</th> <th style="width:150px;">Quantity</th> <th style="width:150px;"> Price</th> <th style="width:150px;"> Total price</th> </tr>');
     
 
      //   for(let sngle of this.OrderViewList.order_item)
      //   {
      //     contentHlmrow=contentHlmrow+'<tbody>';

      //     contentHlmrow=contentHlmrow+'<tr  style="text-align:center;">';
      //     contentHlmrow=contentHlmrow+'<td style="width:250px;">'+sngle.menu_items.lang[this.langindex].name+'</td>';
      //     contentHlmrow=contentHlmrow+'<td style="width:150px;">'+sngle.item_count+'</td>';
      //     contentHlmrow=contentHlmrow+'<td style="width:150px;">'+'SAR'+sngle.item_price+'</td>';
      //     contentHlmrow=contentHlmrow+'<td style="width:150px;">'+'SAR'+sngle.grant_total+'</td>';
      //     contentHlmrow=contentHlmrow+'</tr>';

      //     if(sngle.order_item_addon.length>0)
      //     {
      //       contentHlmrow=contentHlmrow+'<tr>';
      //       contentHlmrow=contentHlmrow+'<td colspan="4"><span>Addons</span></td>';
      //       contentHlmrow=contentHlmrow+'</tr>';
      //     }

      //     if(sngle.order_item_addon.length>0)
      //     {
      //       contentHlmrow=contentHlmrow+'<tr>  <td colspan="4"><table  style="border-collapse: collapse; width: 100%;">';

      //     for(let innersngl of sngle.order_item_addon)
      //     {

      //       contentHlmrow=contentHlmrow+'<tr >';
            

      //       contentHlmrow=contentHlmrow+'<td style="width:250px; text-align:center;">'+innersngl.resturant_addons.lang[this.langindex].name+'</td>';
      //       contentHlmrow=contentHlmrow+'<td style="width:150px; text-align:center;">'+innersngl.item_count+'</td>';
      //       contentHlmrow=contentHlmrow+'<td style="width:150px; text-align:center;">'+'SAR'+innersngl.item_price+'</td>';
      //       contentHlmrow=contentHlmrow+'<td style="width:150px; text-align:center;">'+'SAR'+innersngl.grant_total+'</td>';

      //       contentHlmrow=contentHlmrow+'</tr>';

      //     }

      //     contentHlmrow=contentHlmrow+'</table></td></tr>';
      //     }


      //     contentHlmrow=contentHlmrow+'</tbody>';

      //   }
      //   a.document.write(contentHlmrow);
      //   // a.document.write('<tr></tr><br/>');
      //   // a.document.write('<tr></tr><br/>');
      //   // // a.document.write('<br/>');
      //   a.document.write('<table><br/>');
      //   a.document.write('<tbody><tr>');
      //   a.document.write('<td colspan="1" style="width:250px;text-align:center;font-size:18px; font-weight: bold;">Total amount(VAT Inclusive)</td>');
      //   a.document.write('<td colspan="1" style="width:150px;text-align:center;"><td>');
      //   a.document.write('<td colspan="1" style="width:150px;text-align:center;"><td>');
      //   a.document.write('<td style="width:150px;text-align:center;font-size:18px; font-weight: bold;">'+'SAR'+this.OrderViewList.grand_total+'</td>');
      //   // a.document.write();

      //   a.document.write('</tr></tbody>');
      //   a.document.write('</table>');
      //   a.document.write('</table><br/></br/>');
      //   a.document.write('<div style="margn-top:100px;text-align:center"><span>'+'Powered by Mahally'+'</span></div><br/>');
      
      //   a.document.write('<div style="margn-top:100px;text-align:center"><img src="https://mahally.ourdemo.online/public/frontoffice/assets/images/logo2.png" alt="logo"></div>');
      
      //   a.document.write('</body></html>');
      //   a.document.close();
         

      // }

}
