import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ValidationService } from 'src/app/services/validation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-bump',
  templateUrl: './bump.component.html',
  styleUrls: ['./bump.component.css']
})
export class BumpComponent implements OnInit {

  order_id: number;
  order_status: number;
  update_status: number;
  id: number;
  routerLink;
  langindex:any=0;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    this.route.queryParams.subscribe(params => {
      this.order_id = params['order_id'];
      this.order_status =params['order_status'];
      this.routerLink=params['routerLink'];
      if (this.order_id !== undefined && this.order_status !== undefined) {
        // this.router.navigate(['/pending-orders']);
      }
      else{
      this.router.navigate(['/pending-orders']);
      }
    });
    this.doFetchOrderListByOrderid();
    //console.log(this.id);
  }


  //StausConfirm function

  doUpdateOrderStatusConfirm() {
    this.update_status=2;
    this.apiService.doUpdateOrderStatus(this.id,this.update_status)
      .subscribe(
        (data: any) => {
          this.router.navigate(['/pending-orders']);
          // this.router.navigate(['/confirmed-orders']);
          this.toastr.success(this.langindex==0?'Status changed successfully':'تم تغيير الحالة بنجاح');
          console.log(data)

        },
        error => {
          this.toastr.error(error);
          console.log(error);
        }
      );
  }

    //StausCancel function
  doUpdateOrderStatusCancel() {
    this.update_status=6;
    this.apiService.doUpdateOrderStatus(this.id,this.update_status)
      .subscribe(
        (data: any) => {
          // this.router.navigate(['/confirmed-orders']);
          this.router.navigate(['/pending-orders']);
          this.toastr.success(this.langindex==0?'Status changed successfully':'تم تغيير الحالة بنجاح');
        },
        error => {
          this.toastr.error(error);
          console.log(error);
        }
      );
  }

      //StausCancel function
      doUpdateOrderStatusDelivered() {
        this.update_status=5;
        this.apiService.doUpdateOrderStatus(this.id,this.update_status)
          .subscribe(
            (data: any) => {
              //this.router.navigate(['/order-delivered']);
              this.router.navigate(['/order-ready-for-pickup']);
              this.toastr.success(this.langindex==0?'Status changed successfully':'تم تغيير الحالة بنجاح');
            },
            error => {
              this.toastr.error(error.error.error.msg);
              console.log(error);
            }
          );
      }


      doFetchOrderListByOrderid() {
        this.apiService.doGetOrderListByOrderid(this.order_id)
          .subscribe(
            (data: any) => {
              this.id =data.data.orders[0].id;
              // console.log(data.data.orders[0].id)
            },
            error => {
              this.toastr.error(error.error.error.msg);
              console.log(error.error.error.msg);
            }
          );
          }  

          doViewDetails(){
            this.router.navigate(['/bump-order-confirmation'],{ queryParams:  { order_id: this.order_id,order_status:this.order_status }});
           } 
}
