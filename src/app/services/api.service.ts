import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";
import { environment } from "../../environments/environment";
import { FrontOfficeApiEndPoints } from "./../utils/frontoffice-api-endpoints";
import { BehaviorSubject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private httpOptions: any
  public accessToken: BehaviorSubject<string>
  public userId: string
  private routerInfo: BehaviorSubject<string>;

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
    // logged in so return true
    const user = JSON.parse(localStorage.getItem('currentUser'));
    const atoken = JSON.parse(localStorage.getItem('accessToken'));
    this.accessToken = new BehaviorSubject<string>(JSON.parse(localStorage.getItem('accessToken')));
    this.routerInfo = new BehaviorSubject<string>(JSON.parse(localStorage.getItem('accessToken')));
  }


  setValue(accessToken): void {
    this.routerInfo.next(accessToken);
  }

  public get getTokenValue(): string {
    return this.accessToken.value;
  }

    // Login api call
    doLogin(email: string, password: string) {
      const params = {
        email: email,
        password: password
      };
      return this.http
        .post<any>(`${environment.apiBaseUrl}` + FrontOfficeApiEndPoints.login, params, this.httpOptions)
        .pipe(
          map(user => {
            // console.log('Sucess logn->');
            console.log(user);
            if(user['success']==true){  
            localStorage.setItem("accessToken", JSON.stringify(user['data']['access_token']));
            localStorage.setItem("currentUser", JSON.stringify(user));
            localStorage.setItem("role_id", JSON.stringify(user['data']['user']['role_id']));
            localStorage.setItem("id", JSON.stringify(user['data']['user']['id']));
            localStorage.setItem("restuarants_name_en", (user['data']['user']['lang'][0]['name']));
            localStorage.setItem("restuarants_name_ar", (user['data']['user']['lang'][1]['name']));
             
            this.accessToken.next(user['data']['access_token']);
            
          }
          return user;
          })
        );
    }


  //Forgot Password api
  doFogetPassword(email: string) {
    return this.http
      .post<any>(`${environment.apiBaseUrl}` + FrontOfficeApiEndPoints.forgotpassword, { 'email': email }, this.httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }


  //Verify email api
  doVerify(email: string, code: string) {
    const params = {
      email: email,
      verification_code: code,
    };
    return this.http
      .post<any>(`${environment.apiBaseUrl}` + FrontOfficeApiEndPoints.verify, params, this.httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }


  // Reset Password api call
  doReset(password: string, c_password: string, id: string) {
    const params = {
      password: password,
      c_password: c_password,
      id: id

    };
    return this.http
      .post<any>(`${environment.apiBaseUrl}` + FrontOfficeApiEndPoints.resetpassword, params, this.httpOptions)
      .pipe(
        map(response => {

          return response;

        })
      );
  }


  // Get orderlist
  doGetOrderList(order_status: number) {
    const params = {
      order_status: order_status
    };
    return this.http
      .post<any>(`${environment.apiBaseUrl}` + FrontOfficeApiEndPoints.orderlist, params, this.httpOptions)
      .pipe(
        map(response => {

          return response;

        })
      );
  }

  // Get orderlist by orderid
  doGetOrderListByOrderid(order_id: number) {
    const params = {
      order_id: order_id
    };
    return this.http
      .post<any>(`${environment.apiBaseUrl}` + FrontOfficeApiEndPoints.orderlistsearch, params, this.httpOptions)
      .pipe(
        map(response => {

          return response;

        })
      );
  }
  //OrderViewList By OrdeerBYid
  
  doOrderViewList(id: number) {
    const params = {
      order_id: id
    };
    return this.http
      .post<any>(`${environment.apiBaseUrl}` + FrontOfficeApiEndPoints.OrderViewList, params, this.httpOptions)
      .pipe(
        map(response => {

          return response;

        })
      );
  }



  // Update order status
  doUpdateOrderStatus(id: number, order_status: number) {
    const params = {
      order_status: order_status,
      id: id
    };
    return this.http
      .post<any>(`${environment.apiBaseUrl}` + FrontOfficeApiEndPoints.changestatus, params, this.httpOptions)
      .pipe(
        map(response => {

          return response;

        })
      );
  }

  //Logout
  doLogout() {
    return this.http
      .post<any>(`${environment.apiBaseUrl}` + FrontOfficeApiEndPoints.logout, this.httpOptions)
      .pipe(
        map(response => {
          localStorage.clear();
          return response;
        })
      );
  }

}

