export class KitchenApiEndPoints {
    public static get login(): string { return "kitchen-staff-login"; }
    public static get forgotpassword(): string { return "forgot-password"; }
    public static get verify(): string { return "verify"; }
    public static get resetpassword(): string { return "reset-password"; }
    public static get logout(): string { return "kitchen-logout"; }   
  }